(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .factory('ASearch', ASearch);

    ASearch.$inject = ['$resource'];

    function ASearch($resource) {
        var resourceUrl =  'api/_search/a-s/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
