(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('ADialogController', ADialogController);

    ADialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'A', 'B'];

    function ADialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, A, B) {
        var vm = this;

        vm.a = entity;
        vm.clear = clear;
        vm.save = save;
        vm.bs = B.query({filter: 'a(name)-is-null'});
        $q.all([vm.a.$promise, vm.bs.$promise]).then(function() {
            if (!vm.a.b || !vm.a.b.id) {
                return $q.reject();
            }
            return B.get({id : vm.a.b.id}).$promise;
        }).then(function(b) {
            vm.bs.push(b);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.a.id !== null) {
                A.update(vm.a, onSaveSuccess, onSaveError);
            } else {
                A.save(vm.a, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jhipsterApp:aUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
