(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('AController', AController);

    AController.$inject = ['$scope', '$state', 'A', 'ASearch'];

    function AController ($scope, $state, A, ASearch) {
        var vm = this;
        
        vm.as = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            A.query(function(result) {
                vm.as = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            ASearch.query({query: vm.searchQuery}, function(result) {
                vm.as = result;
            });
        }    }
})();
