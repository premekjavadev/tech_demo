(function() {
    'use strict';
    angular
        .module('jhipsterApp')
        .factory('A', A);

    A.$inject = ['$resource'];

    function A ($resource) {
        var resourceUrl =  'api/a-s/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
