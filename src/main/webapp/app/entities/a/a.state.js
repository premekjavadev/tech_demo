(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('a', {
            parent: 'entity',
            url: '/a',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'AS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/a/a-s.html',
                    controller: 'AController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('a-detail', {
            parent: 'entity',
            url: '/a/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'A'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/a/a-detail.html',
                    controller: 'ADetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'A', function($stateParams, A) {
                    return A.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'a',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('a-detail.edit', {
            parent: 'a-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a/a-dialog.html',
                    controller: 'ADialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['A', function(A) {
                            return A.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('a.new', {
            parent: 'a',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a/a-dialog.html',
                    controller: 'ADialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('a', null, { reload: 'a' });
                }, function() {
                    $state.go('a');
                });
            }]
        })
        .state('a.edit', {
            parent: 'a',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a/a-dialog.html',
                    controller: 'ADialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['A', function(A) {
                            return A.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('a', null, { reload: 'a' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('a.delete', {
            parent: 'a',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/a/a-delete-dialog.html',
                    controller: 'ADeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['A', function(A) {
                            return A.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('a', null, { reload: 'a' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
