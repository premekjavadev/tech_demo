(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('ADetailController', ADetailController);

    ADetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'A', 'B'];

    function ADetailController($scope, $rootScope, $stateParams, previousState, entity, A, B) {
        var vm = this;

        vm.a = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jhipsterApp:aUpdate', function(event, result) {
            vm.a = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
