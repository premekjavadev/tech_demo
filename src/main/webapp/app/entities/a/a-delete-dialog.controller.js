(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('ADeleteController',ADeleteController);

    ADeleteController.$inject = ['$uibModalInstance', 'entity', 'A'];

    function ADeleteController($uibModalInstance, entity, A) {
        var vm = this;

        vm.a = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            A.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
