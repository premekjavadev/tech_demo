(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('BController', BController);

    BController.$inject = ['$scope', '$state', 'B', 'BSearch'];

    function BController ($scope, $state, B, BSearch) {
        var vm = this;
        
        vm.bs = [];
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            B.query(function(result) {
                vm.bs = result;
            });
        }

        function search () {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            BSearch.query({query: vm.searchQuery}, function(result) {
                vm.bs = result;
            });
        }    }
})();
