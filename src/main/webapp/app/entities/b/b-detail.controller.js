(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('BDetailController', BDetailController);

    BDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'B', 'A'];

    function BDetailController($scope, $rootScope, $stateParams, previousState, entity, B, A) {
        var vm = this;

        vm.b = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('jhipsterApp:bUpdate', function(event, result) {
            vm.b = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
