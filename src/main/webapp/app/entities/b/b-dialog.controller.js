(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('BDialogController', BDialogController);

    BDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'B', 'A'];

    function BDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, B, A) {
        var vm = this;

        vm.b = entity;
        vm.clear = clear;
        vm.save = save;
        vm.as = A.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.b.id !== null) {
                B.update(vm.b, onSaveSuccess, onSaveError);
            } else {
                B.save(vm.b, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('jhipsterApp:bUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
