(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .factory('BSearch', BSearch);

    BSearch.$inject = ['$resource'];

    function BSearch($resource) {
        var resourceUrl =  'api/_search/b-s/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
