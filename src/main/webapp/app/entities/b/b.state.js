(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('b', {
            parent: 'entity',
            url: '/b',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'BS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/b/b-s.html',
                    controller: 'BController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('b-detail', {
            parent: 'entity',
            url: '/b/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'B'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/b/b-detail.html',
                    controller: 'BDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'B', function($stateParams, B) {
                    return B.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'b',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('b-detail.edit', {
            parent: 'b-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/b/b-dialog.html',
                    controller: 'BDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['B', function(B) {
                            return B.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('b.new', {
            parent: 'b',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/b/b-dialog.html',
                    controller: 'BDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('b', null, { reload: 'b' });
                }, function() {
                    $state.go('b');
                });
            }]
        })
        .state('b.edit', {
            parent: 'b',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/b/b-dialog.html',
                    controller: 'BDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['B', function(B) {
                            return B.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('b', null, { reload: 'b' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('b.delete', {
            parent: 'b',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/b/b-delete-dialog.html',
                    controller: 'BDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['B', function(B) {
                            return B.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('b', null, { reload: 'b' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
