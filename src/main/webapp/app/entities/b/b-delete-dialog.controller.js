(function() {
    'use strict';

    angular
        .module('jhipsterApp')
        .controller('BDeleteController',BDeleteController);

    BDeleteController.$inject = ['$uibModalInstance', 'entity', 'B'];

    function BDeleteController($uibModalInstance, entity, B) {
        var vm = this;

        vm.b = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            B.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
