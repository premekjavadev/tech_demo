(function() {
    'use strict';
    angular
        .module('jhipsterApp')
        .factory('B', B);

    B.$inject = ['$resource'];

    function B ($resource) {
        var resourceUrl =  'api/b-s/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
