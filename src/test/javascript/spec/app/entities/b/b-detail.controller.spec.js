'use strict';

describe('Controller Tests', function() {

    describe('B Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockB, MockA;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockB = jasmine.createSpy('MockB');
            MockA = jasmine.createSpy('MockA');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'B': MockB,
                'A': MockA
            };
            createController = function() {
                $injector.get('$controller')("BDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'jhipsterApp:bUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
