'use strict';

describe('Controller Tests', function() {

    describe('A Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockA, MockB;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockA = jasmine.createSpy('MockA');
            MockB = jasmine.createSpy('MockB');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'A': MockA,
                'B': MockB
            };
            createController = function() {
                $injector.get('$controller')("ADetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'jhipsterApp:aUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
